export interface ApplicationType {
    id: string;
    companyName: string;
    name: string;
    position: string;
    email: string;
    phoneNumber: string;
    stationDescription: string;
    comment: string;
}