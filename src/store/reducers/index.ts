import {combineReducers} from 'redux';
import applicationsReducer from './applicationsReducer';

export default combineReducers({
    applicationsReducer
})
