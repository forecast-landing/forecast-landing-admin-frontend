import * as types from '../actions/types';
import { ApplicationType } from "@/types";

interface ApplicationsState {
    isLoading: boolean;
    applications: ApplicationType[];
  }

const initialState: ApplicationsState = {
    isLoading: false,
    applications: [],
};

export default function booksReducers(state=initialState, action: any) {
    switch(action.type) {
        case types.SUCCESS_GET_APPLICATIONS: 
            return {...state,applications:action.payload.data};
        default:
            return state;
    }
}
