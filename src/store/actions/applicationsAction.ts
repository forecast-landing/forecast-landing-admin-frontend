import * as types from './types';
import { ApplicationType } from "@/types";

export function getApplications(data: ApplicationType[]) {
    return {data,type:types.GET_APPLICATIONS};
}
