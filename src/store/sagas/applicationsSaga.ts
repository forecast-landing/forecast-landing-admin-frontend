import {all , put , takeLatest} from 'redux-saga/effects';
import * as types from '../actions/types';
import axios from 'axios'

function* getApplications(): Generator<any, void, any>{
    try {
        const APPLICATIONS = yield axios.get(`https://api-landing.intech-forecast.com/applications`);
        yield put({
                type: types.SUCCESS_GET_APPLICATIONS,
                payload: APPLICATIONS
            })
    } catch(e) {
        yield put({type:types.FAILURE_GET_APPLICATIONS, errors:e})
    }
}

export function* applicationsSaga(): Generator{
    yield all([
        yield takeLatest(types.GET_APPLICATIONS, getApplications)
    ])
}
