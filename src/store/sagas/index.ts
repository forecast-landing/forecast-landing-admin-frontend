import {all} from 'redux-saga/effects';
import {applicationsSaga} from './applicationsSaga';

export default function* rootSaga(){
    yield all([
        applicationsSaga()
    ])
}
