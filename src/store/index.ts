import { Middleware, applyMiddleware, createStore } from 'redux';
import { createLogger, ReduxLoggerOptions } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';

import reducers from './reducers';
import rootSaga from './sagas';

const logger = createLogger({
  collapsed: true,
});

export default function configureStore(initialState: any) {
  const sagaMiddleware = createSagaMiddleware();

  const getMiddleware = () => {
    if (process.env.NODE_ENV === 'development') {
      const composeEnhancers = composeWithDevTools({
      });
      return composeEnhancers(applyMiddleware(sagaMiddleware, logger as Middleware));
    }
    return applyMiddleware(sagaMiddleware);
  };

  const store = createStore(
    reducers, // Ensure you pass your combined reducers here
    initialState,
    getMiddleware()
  );

  sagaMiddleware.run(rootSaga);
  return store;
}
