import type { Metadata } from "next";
import "./globals.css";
import AppProvider from "./AppProvider";

export const metadata: Metadata = {
  title: "Админская панель",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <head>
      <link
        rel="icon"
        href="/icon?<generated>"
        type="image/<generated>"
        sizes="<generated>"
      />
      </head>
      <AppProvider>
        <body>{children}</body>
      </AppProvider>
    </html>
  );
}
