import ApplicationList from "@/components/ApplicationList"
import ApplicationContent from "@/components/ApplicationContent"
import { ApplicationType } from "@/types"

const applications: ApplicationType[] = [
    { 
      id: '1', 
      companyName: 'Компания 1', 
      name: 'Имя 1', 
      position: 'Должность 1', 
      email: 'email1@example.com', 
      phoneNumber: '123-456-789', 
      stationDescription: 'Описание станции 1', 
      comment: 'Комментарий 1' 
    },
    { 
      id: '2', 
      companyName: 'Компания 2', 
      name: 'Имя 2', 
      position: 'Должность 2', 
      email: 'email2@example.com', 
      phoneNumber: '987-654-321', 
      stationDescription: 'Описание станции 2', 
      comment: 'Комментарий 2' 
    },
    { 
      id: '3', 
      companyName: 'Компания 3', 
      name: 'Имя 3', 
      position: 'Должность 3', 
      email: 'email3@example.com', 
      phoneNumber: '555-123-456', 
      stationDescription: 'Описание станции 3', 
      comment: 'Комментарий 3' 
    },
  ];

export default function Application(
    {params} : 
    { params : {id : string}}
) {
    return (
        <div className="flex h-screen">
            <ApplicationList id = {params.id}/>
            <main className="flex-1 p-8 bg-gray-100 overflow-y-auto">
                <ApplicationContent id = {params.id}/>
            </main>
        </div>
    )
}