'use client'
import Link from "next/link";
import { ApplicationType } from "@/types";
import { connect } from 'react-redux';
import { getApplications } from "@/store/actions/applicationsAction";
import {Dispatch, bindActionCreators} from 'redux';
import { useEffect } from "react";

function ApplicationList(
    { id , getApplicationsAction, applications} :
    { id? : string, getApplicationsAction: any, applications: ApplicationType[]}
) {
    useEffect(() => {
      if (applications.length === 0) {
        getApplicationsAction();
      }
    
    }, [])
    return (
        <aside className="w-64 bg-gray-800 text-white overflow-y-auto">
          <ul className="p-4">
            {applications.map((app) => (
              <li key={app.id} className="mb-2">
                <Link  className={`block p-2 rounded ${app.id === id ? 'bg-blue-500 text-white' : 'hover:bg-gray-700'}`} href={`/${app.id}`}>
                    {app.companyName}
                </Link>
              </li>
            ))}
          </ul>
        </aside>
    );
}

const mapDispatchToProps = (dispatch: Dispatch) =>({
  getApplicationsAction:bindActionCreators(getApplications,dispatch),
})

const mapStateToProps = (state: any) => ({
  applications: state.applicationsReducer.applications
})

export default connect(mapStateToProps,mapDispatchToProps)(ApplicationList);

