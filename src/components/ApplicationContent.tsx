'use client'
import { ApplicationType } from "@/types";
import { connect } from 'react-redux';
import { getApplications } from "@/store/actions/applicationsAction";
import {Dispatch, bindActionCreators} from 'redux';
import { useEffect, useState } from "react";
import sunStation from '../assets/sun-station.png';
import Image from 'next/image';

function ApplicationContent(
    { id , getApplicationsAction, applications} :
    { id? : string, getApplicationsAction: any, applications: ApplicationType[]}
) {
    const [application, setApplication] = useState<ApplicationType | undefined>(undefined);
    useEffect(() => {
        if (applications.length == 0) {
            getApplicationsAction();
        }
    }, []);
    useEffect(() => {
        setApplication(applications.find(app => app.id == id));
    },[applications])
    return (
        <div className="p-4 bg-white shadow rounded-md">
            {application ? (
                <>
                    <h1 className="text-2xl font-bold mb-4">{application.name}</h1>
                    <div className="flex justify-between items-center">
                        <p className="text-sm text-gray-500">Компания: {application.companyName}</p>
                        <p className="text-sm text-gray-500">Позиция: {application.position}</p>
                    </div>
                    <div className="flex justify-between items-center mt-2">
                        <p className="text-sm text-gray-500">Электронная почта: {application.email}</p>
                        <p className="text-sm text-gray-500">телефон: {application.phoneNumber}</p>
                    </div>
                    <div className="mt-4">
                        <p className="text-gray-700">Описание станции: {application.stationDescription}</p>
                    </div>
                    <div className="mt-4">
                        <p className="text-gray-700">Комментарий: {application.comment}</p>
                    </div>
                </>
            ) : <Image src = {sunStation} alt="" className="w-[100%]"/>}
        </div>
    );
}


const mapDispatchToProps = (dispatch: Dispatch) =>({
    getApplicationsAction:bindActionCreators(getApplications,dispatch),
  })
  
  const mapStateToProps = (state: any) => ({
    applications: state.applicationsReducer.applications
  })
  
  export default connect(mapStateToProps,mapDispatchToProps)(ApplicationContent);